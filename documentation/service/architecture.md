# Resource Manager (RM)

The Resource Manager allows you to:
 - manipulate contexts (create, activate or deactivate, delete, etc.);
 - add to a Context any resources.

The Resource Manager (RM) does not have its own persistence, instead it uses the Information System (IS), via the Resource Registry Service, as persistence to store information and to get the knowledge to accomplish the above mentioned actions.

The Resource Manager is designed to deal with the gCube Model. 
This means that it knows the semantic of certain defined types of entities (**Resource** and **Facet**) and relations (**IsRelatedTo** and **ConsistsOf**) and is capable of performing specific actions according to the knowledge stored in the IS.

The gCube Model is a specialization of the Information System Model which is a graph model.

```mermaid
graph LR
  Resource(<strong>Resource</strong>):::r
  Facet(<strong>Support</strong>):::f
  Resource ==>|<strong>IsRelatedTo</strong>| Resource
  Resource ==>|<strong>ConsistsOf</strong>| Facet
   
  classDef r fill:#4f81bd,stroke:#385d8a,stroke-width:2px;
  classDef f fill:#9bbb59,stroke:#4f6228,stroke-width:2px;
  linkStyle default stroke:#385d8a,fill:none;
  linkStyle 1 stroke:#9bbb59,fill:none;
```

We will design the graph as shown in the diagram above. In particular, we will design any:

* **Resource** as a <span style="color:#4f81bd;">**blue**</span> node indicating the type of the resource in bold and eventually any other information in the lines below;
* **Facet** as a <span style="color:#9bbb59;">**green**</span> node indicating the type of the facet in bold and eventually any other information in the lines below;
* **IsRelatedTo** as a <span style="color:#4f81bd;">**blue**</span> edge indicating the type of the relation in bold and eventually any other information in the lines below;
* **ConsistsOf** as a <span style="color:#9bbb59;">**green**</span> edge indicating the type of the relation in bold and eventually any other information in the lines below.


## Context Manipulation

...

## Add Resources to Context

In this section, we will cover the aspect of adding resources to a context.

Any resource that is part of the **AddToContext** operation will be analyzed based on its type.

The specific type support has been developed in a pluggable way. 
Whenever we identify the need to support a specific type, the service can be extended by adding a plugin instead of modifying the core of the service.

The specific behaviour to **AddToContext** a specific instace must be defined in the knowledge base (i.e. in the IS). 

It is the responsibility of the plugin supporting the type to:
- define a plan to provide to the ResourceManager;  
- execute the provided plan.

Please note that there could be more than one plugin for the same type. 
Two plugins for the same type are executed in alphabetical order.

Only the plugin can understand and perform the operation.

Any plugin analyzing a resource can export variables which in turn can be used by any other plugins in the analysis of the instances.
The variables that plugins can export are defined in the plan.

Any plugin analyzes the Related Resource in the plan and performs its actions independently from the others.

Every plugin can identify a set of Related Resources, 
i.e. resources which will be involved in the *AddToContext* operation due to:

- propagation constraints. The *add* propagation constraint of the outgoing *IsRelatedTo* relation is set to **propagate**. We will refer to these resources as **Mandatory Related Resources (M-RR)**.
  

  > Propagation Constraints: The rules that determines how changes (i.e. add to context, remove from context, delete) to a resource are propagated to related resources.

  This is an example of **M-RR**

  ```mermaid
  graph LR
      cvs(<strong>VirtualService</strong><br/>catalogue-virtual-service):::r 
      ct(<strong>ConfigurationTemplate</strong><br/>catalogue-configuration-template):::r
      
      cvs ==>|<strong>IsCustomizedBy</strong><br />add=propagate<br />remove=propagate| ct

      classDef r fill:#4f81bd,stroke:#385d8a,stroke-width:2px;
      classDef f fill:#9bbb59,stroke:#4f6228,stroke-width:2px;
      linkStyle default stroke:#385d8a,fill:none;
  ```

  Please note that only the first level of **M-RR** must be considered from every plugin.

- derivated knowledge as part of plugin elaboration. We will refer to these resources as **Derivated Related Resources (D-RR)**.

  **D-RR** could be at any level from the originated resources. It is responsibility of the plugin to identify them.




The Resource Manager provides a support to generate the derivated knowledge using *DiscoveryFacet*.

DiscoveryFacet defines a group of resources to be selected. In particular the facet defines:
- **name**: The name of the 'group' of resources to discover;
- **description**: the description to display for the group of discovered resources;
- **min** and **max**: defines if it is mandatory selecting of the the resources (whne min=1, min=0 means not mandatory) and if multiple choiche is allowed (when max>1, max=null means unlimited);
- **queries**: a list of JSON Querys to invoke to retrieve the resources to display. The list of resources is the union of the results of the queries;
- **queryTemplates**: a list JSON Query Templates (with the actual argument to provide) to retrieve the resources to display. The list of resources is the union of the results of the queries.


```json 
{
    "type": "ConsistsOf",
    "id": "76f99360-a952-4164-9655-34e19f090bab",
    "propagationConstraint": {
        "type": "PropagationConstraint",
        "add": "propagate",
        "delete": "cascade",
        "remove": "cascade",
    },
    "target": {
        "type": "DiscoveryFacet",
        "id": "14f6cce5-737f-4199-a3f4-fcd327599de0",
        "name": "A name for the discovered instances",
        "description": "A description fo the discovered instances to select.",
        "min": 1,
        "max": 1,
        "queries": [
          {
              "type": "HostingNode",
              "consistsOf": [
                  {
                      "type": "ConsistsOf",
                      "target": {
                          "type": "StateFacet",
                          "value": "certified"
                      }
                  }
              ]
          }
        ],
        "queryTemplates": [
          {
            "name" : "name-of-the-query-template-to-use"
            "variables": {
              "name" : "the-name"
            }
          }
        ]
    }
}
```
> **TODO** Define *DiscoveryFacet*

The Resource Manager allows to use some variable in any field of the Discovery Facet as it does for the variables defined for any instance of the plugin.
It is responsibility of the Resource Manager to replace the variable with the current value.

In particular, it allows to use the following variables:
- ${resource} : identify the resource that the *DiscoveryFacet* is describing;
- ${consistsOf}: identify the consistsOf used to relate the *DiscoveryFacet* with the resource;
- ${facet}: identify the *DiscoveryFacet* itself.
- ${source}: can be null. If the resource that the *DiscoveryFacet* is describing is under analyssi because is a related resource this variable identify such a resource.

It is expected that ${resource} ans ${source} are useful.

For any of these variables any field of the identified entity or relation can be referred usign dot notation.
Examples
- ${resource.id} will be the id of the resource the *DiscoveryFacet* is describing;
- ${facet.name} is "A name for the discovered instances, e.g. CKan instances".

The identified related resources are in turn analysed by the dedicated plugin (if any).
The Resource Manager maintains a list of elaborated resources and will skip the instances already analysed.
It also maintains the depencies relation, i.e. the source of the related resources.

Currently, the Resource Manager has built-in defined plugins for the following types:
- VirtualServices;
- ConfigurationTemplate;
- EService indentifing a Smartgears nodes (i.e. it has an incoming *Hosts* relation from an *HostingNode*).

<!--
The Resource Manager allows the user to perform some choice in a coherent and organized way to properly add a service to a Context hopefully with no extra manual action required from the requesting user.
-->

Please note that when a new context is created the list of initial resources to be analysed are idenfied by invoking a query template installed in the IS by the Resource Manager itself.

In particular, the ResourceManager invoke [get-virtual-service](../../main/resources/get-virtual-service.json) query template.

The query template is as following 

```json
{
  "type" : "VirtualService"
}
```

This query could changed the future.


### VirtualServices

*VirtualService* are used to define services as aggregation of effective service, configurations, datasets etc.

The plugin for the VirtualServices extracts the information which allows to create the knowledge to:
- define the list of related resources (and relations) to be added to the Context via the IS;
- perform actions which has an effect outside of the IS, e.g. deploy a new docker instance or invoke an ansible playbook.

Each *VirtualService* is defined by:

* IsIdentifiedBy -> SoftwareFacet which has information such as:
  - **group**: the group is used in the Context Modeler GUI;
  - **name**: the name to present the service in the GUI;
  - **description**: the description to present the service in the GUI;
  - **optional**: the service is mandatory (when <code>false</code>) for a new Context  or optional (when <code>true</code>).
    The mandatory services will be presented with a selected checkbox which cannot be unselected.

  ```json
  {
      "type": "VirtualService",
      "id": "bd2a1f51-da7c-424c-8ce1-e510e8f928e9",
      "consistsOf": [
          {
              "type": "IsIdentifiedBy",
              "id": "4c5bbe66-2fd3-4017-8ebd-605a75061588",
              "propagationConstraint": {
                  "type": "PropagationConstraint",
                  "add": "propagate",
                  "delete": "cascade",
                  "remove": "cascade",
              },
              "target": {
                  "type": "SoftwareFacet",
                  "id": "22868a1f-d68c-424f-b963-11b316ebad49",
                  "group": "Data Discovery and Access",
                  "name": "Resource Catalogue",
                  "version": "1.0.0",
                  "description": "This functionality provides VRE members with a resource catalogue. All the Catalogue items are accompanied with rich descriptions capturing general attributes, e.g. title and creator(s); accessibility properties; technical properties, e.g. size and format; legal and ethical attributes, e.g. whether containing personal data; use and reuse policies, e.g. licences.",
                  "optional": true
              }
          }
      ]
  }
  ```

* ConsistsOf -> DiscoveryFacet (0..n) 
  
  These facets discover a group of related resources. 

  ```json 
  {
      "type": "ConsistsOf",
      "id": "b8e16e0c-eead-445c-98b9-618c7a59ec5c",
      "propagationConstraint": {
          "type": "PropagationConstraint",
          "add": "propagate",
          "delete": "cascade",
          "remove": "cascade",
      },
      "target": {
          "type": "DiscoveryFacet",
          "id": "579ec5dd-5f97-498c-b828-b230d8120dbc",
          "name": "Ckan Instances",
          "description": "The Ckan instance to use for the Resource Catalogue",
          "min": 1,
          "max": 1,
          "queries": [
            {
              "type": "EService",
              "consistsOf": [
                {
                  "type": "IsIdentifiedBy",
                  "target" : {
                    "type" : "SoftwareFacet",
                    "name": "ckan"
                  }
                }
              ]
              "isRelatedTo": [
                {
                  "type": "CallsFor",
                  "source": {
                    "type": "VirtualService",
                    "id": "${resource.id}"
                  }
                }
              ]
            }
          ]
      }
  }
  ```

  Please note that <code>${resource.id}</code> is the id of the resource that the DiscoveryFacet is describing.


* ConsistsOf -> ActionFacet (0..n) 
  
  The facets define a list of actions to invoke independently from the related resources selected. 
  If the service is selected, these action must be executed (if optional is false). 
  The GUI should show the optional action as selection.

    ```json 
    {
        "type": "ConsistsOf",
        "id": "67fed454-43a6-4c33-8d1a-965af1496d87",
        "propagationConstraint": {
            "type": "PropagationConstraint",
            "add": "propagate",
            "delete": "cascade",
            "remove": "cascade",
        },
        "target": {
            "type": "ActionFacet",
            "id": "63ed98b7-301e-4ac4-a610-449db81c4893",
            "name": "",
            "description": "",
            "optional": false,
            ....
        }
    }
    ```
  > **TODO** Review *ActionFacet* definition


### ConfigurationTemplate

For each related *ConfigurationTemplate* in the plan are performed the following actions:

  - An instance of *Configuration* is created with one Facet for each defined TemplateFacet int the ConfigurationTemplate. The type fo the created Facet and it own properties depends on the definition of the TemplateFacet. 
    It is responsibility of the user, provide the values of the properties. The plugin for *ConfigurationTemplate*
    check that the constraints are satisfied.

  - A relation *IsDerivationOf* from the created *Configuration* to the source *ConfigurationTemplate* is created.
    The propagation constraints will be:
    - add: unpropagate;
    - remove: keep;
    - delete: keep.

  ```mermaid
  graph LR
      cvs(<strong>VirtualService</strong><br/>catalogue-virtual-service):::r 
      ct(<strong>ConfigurationTemplate</strong><br/>catalogue-configuration-template):::r
      tf(<strong>TemplateFacet</strong><br/>targetFacetType:SimpleFacet):::f
      
      
      cvs ==>|<strong>IsCustomizedBy</strong><br />add=propagate<br />remove=propagate| ct
      ct ==>|<strong>ConsistsOf</strong>| tf
      
      classDef r fill:#4f81bd,stroke:#385d8a,stroke-width:2px;
      classDef f fill:#9bbb59,stroke:#4f6228,stroke-width:2px;
      linkStyle default stroke:#385d8a,fill:none;
  ```

  For the above example the final result will be something like the following:

  ```mermaid
  graph LR
      cvs(<strong>VirtualService</strong><br/>catalogue-virtual-service):::r 
      ct(<strong>ConfigurationTemplate</strong><br/>catalogue-configuration-template):::r
      tf(<strong>TemplateFacet</strong><br/>targetFacetType:SimpleFacet):::f
      
      c(<strong>Configuration</strong><br/>catalogue-configuration):::r
      sf(<strong>SimpleFacet</strong>):::f

      cvs ==>|<strong>IsCustomizedBy</strong><br />add=propagate<br />remove=propagate| ct
      ct ==>|<strong>ConsistsOf</strong>| tf
      cvs ==>|<strong>IsCustomizedBy</strong><br />add=propagate<br />remove=propagate| c
      c ==>|<strong>ConsistsOf</strong>| sf
      c ==> |<strong>IsDerivationOf</strong><br />add=unpropagate<br />remove=keep| ct

      
      classDef r fill:#4f81bd,stroke:#385d8a,stroke-width:2px;
      classDef f fill:#9bbb59,stroke:#4f6228,stroke-width:2px;
      linkStyle default stroke:#385d8a,fill:none;
      linkStyle 2,4 stroke:#9bbb59,fill:none;
  ```

### EServices

For each *EService* indentifing a Smartgears nodes (i.e. it has an incoming *Hosts* relation from an *HostingNode*) it is invoked the AddToContext action to the smartgears node.


### Example of Virtual Service and its related Resources

```mermaid
graph LR
    cvs(<strong>VirtualService</strong><br/>catalogue-virtual-service):::r 
    ct(<strong>ConfigurationTemplate</strong><br/>catalogue-configuration-template):::r
    tf(<strong>TemplateFacet</strong><br/>targetFacetType:SimpleFacet):::f
    gcat(<strong>EService</strong><br/>gcat):::r
    gcat-hn(<strong>HostingNode</strong><br/>gcat.d4science.org):::r

    gcat-hn ==>|<strong>Hosts</strong><br />add=propagate<br />remove=propagate| gcat
    cvs ==>|<strong>CallsFor</strong><br />add=unpropagate<br />remove=propagate| gcat
    
    cvs ==>|<strong>IsCustomizedBy</strong><br />add=propagate<br />remove=propagate| ct
    ct ==>|<strong>ConsistsOf</strong>| tf
  
    ckan1(<strong>EService</strong><br/><strong>id</strong>:8b664db9-dc32-4af9-9812-3ba794d2712d><br/><strong>name</strong>:ckan):::r
    pg1(<strong>EService</strong><br/>postgres-ckan-db-1):::r
    slr1(<strong>EService</strong><br/>solr-1):::r
    
    cvs ==>|<strong>CallsFor</strong><br />add=unpropagate<br />remove=propagate<br />| ckan1
    ckan1 ==>|<strong>Uses</strong><br />add=propagate<br />remove=propagate| pg1
    ckan1 ==>|<strong>Uses</strong><br />add=propagate<br />remove=propagate| slr1

    ckan2(<strong>EService</strong><br/><strong>id</strong>:2a360a1f-2889-40b5-af2b-129aa60c3ca3><br/><strong>name</strong>:ckan):::r
    pg2(<strong>EService</strong><br/>postgres-ckan-db-2):::r
    slr2(<strong>EService</strong><br/>solr-2):::r

    cvs ==>|<strong>CallsFor</strong><br />add=unpropagate<br />remove=propagate<br />| ckan2
    ckan2 ==>|<strong>Uses</strong><br />add=propagate<br />remove=propagate| pg2
    ckan2 ==>|<strong>Uses</strong><br />add=propagate<br />remove=propagate| slr2

    classDef r fill:#4f81bd,stroke:#385d8a,stroke-width:2px;
    classDef f fill:#9bbb59,stroke:#4f6228,stroke-width:2px;
    linkStyle default stroke:#385d8a,fill:none;
    linkStyle 3 stroke:#9bbb59,fill:none;
```

The **VirtualService** for the catalogue (i.e. ***catalogue-virtual-service***):

- provides the way to instantiate a **Configuration**: this is done with the **ConfigurationTemplate** and its **TemplateFacet**.
  In the example, the **TemplateFacet** defines that a **SimpleFacet** must be instantiated. 
  The **TemplateFacet** also defines the list of attributes and their types.
  The logic of the **ResourceManager** will instantiate a **Configuration** with attached the instantiate Facet as defined in the **TemplateFacet**.
  Moreover, the logic of the **ResourceManager** will create an **IsDerivationOf** relation between the created **Configuration** and the **ConfigurationTemplate**.
  In the future, we have to investigate if we can remove this hard-coded logic from the **ResourceManager**.

```mermaid
graph LR
    cvs(<strong>VirtualService</strong><br/>catalogue-virtual-service):::r 
    
    ct(<strong>ConfigurationTemplate</strong><br/>catalogue-configuration-template):::r
    tf(<strong>TemplateFacet</strong><br/>targetFacetType:SimpleFacet):::f
    
    c(<strong>Configuration</strong><br/>catalogue-configuration):::r
    sf(<strong>SimpleFacet</strong>):::f

    
    cvs ==>|<strong>IsCustomizedBy</strong><br />add=propagate<br />remove=propagate| ct
    ct ==>|<strong>ConsistsOf</strong>| tf
    cvs ==>|<strong>IsCustomizedBy</strong><br />add=propagate<br />remove=propagate| c
    c ==>|<strong>ConsistsOf</strong>| sf
    c ==> |<strong>IsDerivationOf</strong><br />add=unpropagate<br />remove=keep| ct

    classDef r fill:#4f81bd,stroke:#385d8a,stroke-width:2px;
    classDef f fill:#9bbb59,stroke:#4f6228,stroke-width:2px;
    linkStyle default stroke:#385d8a,fill:none;
    linkStyle 1,3 stroke:#9bbb59,fill:none;
```

>  As general rules:
>  - when there is an **IsRelatedTo** relation starting from the **VirtualService** with **add** propagation constraint set to ***unpropagate***, then the **VRE Modeler GUI** **MUST** propose the target Resource as an option to be added to the target Context.
The ***addToContext*** operation will be explicitly invoked to the *IsRelatedTo* from the **ResourceManager** relation for the user selected resources, apart from the ***VirtualService*** itself. This logic will be defined in the plan.
> - when instead the **add** propagation constraint is set to ***propagate*** the target Resource will be show as mandatory. It could not be deselected.

> In the future, we have to find a way to define a mandatory resource even if the **add** propagation constraint is set to ***unpropagate*** . In the example, I want of of the **gcat** instances and I want one fo the **ckan** instances.


- is related to one or more ***gcat*** instances already available in the current context. 
  The current context is the source context of the ***addToContext*** operation.
  The target context of the ***addToContext*** operation is the context where the **VirtualService** is going to be added.
  
  in teh example above, please note that:
  - the **CallsFor** relation between ***catalogue-virtual-service*** and ***gcat*** has the **add** propagation constraint set to ***unpropagate***. For this reason the user must explictly select **gcat**.
  - The **ResourceManager** logic will also invoke ***addToContext*** to the Smartgears node.
    The **ResourceManager** understand that is a Smartgears service because the **EService** has an incoming **Hosts** relation.
    In the future we have to investigate if we can remove this hard-coded logic from the **ResourceManager**. 
    This action (i.e. ***addToContext*** to the Smartgears node) could be defined in the **ActionFacet** attached to the **VirtualService**.
    The **ActionFacet** could be automatically created from the service itself at startup. Please note that the service at startup must also create the **CallsFor** relation with the ***catalogue-virtual-service***.

```mermaid
graph LR
    cvs(<strong>VirtualService</strong><br/>catalogue-virtual-service):::r 
    
    gcat(<strong>EService</strong><br/>gcat):::r
    gcat-hn(<strong>HostingNode</strong><br/>gcat.d4science.org):::r

    gcat-hn ==>|<strong>Hosts</strong><br />add=propagate<br />remove=propagate| gcat
    cvs ==>|<strong>CallsFor</strong><br />add=unpropagate<br />remove=propagate| gcat

    classDef r fill:#4f81bd,stroke:#385d8a,stroke-width:2px;
    classDef f fill:#9bbb59,stroke:#4f6228,stroke-width:2px;
    linkStyle default stroke:#385d8a,fill:none;
```

- the ***catalogue-virtual-service*** has **CallsFor** relations with one or more ***ckan*** instances. For this reason the the **add** propagation constraint is set to ***unpropagate***.

```mermaid
graph LR
    cvs(<strong>VirtualService</strong><br/>catalogue-virtual-service):::r 

    ckan2(<strong>EService</strong><br/>ckan-2):::r
    pg2(<strong>EService</strong><br/>postgres-ckan-db-2):::r
    slr2(<strong>EService</strong><br/>solr-2):::r

    cvs ==>|<strong>CallsFor</strong><br />add=unpropagate<br />remove=propagate<br />| ckan2
    ckan2 ==>|<strong>Uses</strong><br />add=propagate<br />remove=propagate| pg2
    ckan2 ==>|<strong>Uses</strong><br />add=propagate<br />remove=propagate| slr2

    classDef r fill:#4f81bd,stroke:#385d8a,stroke-width:2px;
    classDef f fill:#9bbb59,stroke:#4f6228,stroke-width:2px;
    linkStyle default stroke:#385d8a,fill:none;

```

For the example above the **ResourceManager GUI** will propose as an option to add:
- **gcat**
- **ckan-1**
- **ckan-2**

We imagine that the user selected **gcat** and **ckan-2** instances. 

#### Virtual service added to target Context 

This will be the resulting graph in the target Context.


```mermaid
graph LR
    cvs(<strong>VirtualService</strong><br/>catalogue-virtual-service):::r 
    
    ct(<strong>ConfigurationTemplate</strong><br/>catalogue-configuration-template):::r
    tf(<strong>TemplateFacet</strong><br/>targetFacetType:SimpleFacet):::f
    
    c(<strong>Configuration</strong><br/>catalogue-configuration):::r
    sf(<strong>SimpleFacet</strong>):::f

    gcat(<strong>EService</strong><br/>gcat):::r
    gcat-hn(<strong>HostingNode</strong><br/>gcat.d4science.org):::r

    gcat-hn ==>|<strong>Hosts</strong><br />add=propagate<br />remove=propagate| gcat
    cvs ==>|<strong>CallsFor</strong><br />add=unpropagate<br />remove=propagate| gcat

    cvs ==>|<strong>IsCustomizedBy</strong><br />add=propagate<br />remove=propagate| ct
    ct ==>|<strong>ConsistsOf</strong>| tf
    cvs ==>|<strong>IsCustomizedBy</strong><br />add=propagate<br />remove=propagate| c
    c ==>|<strong>ConsistsOf</strong>| sf
    c ==> |<strong>IsDerivationOf</strong><br />add=unpropagate<br />remove=keep| ct

    ckan2(<strong>EService</strong><br/>ckan-2):::r
    pg2(<strong>EService</strong><br/>postgres-ckan-db-2):::r
    slr2(<strong>EService</strong><br/>solr-2):::r

    cvs ==>|<strong>CallsFor</strong><br />add=unpropagate<br />remove=propagate<br />| ckan2
    ckan2 ==>|<strong>Uses</strong><br />add=propagate<br />remove=propagate| pg2
    ckan2 ==>|<strong>Uses</strong><br />add=propagate<br />remove=propagate| slr2

    classDef r fill:#4f81bd,stroke:#385d8a,stroke-width:2px;
    classDef f fill:#9bbb59,stroke:#4f6228,stroke-width:2px;
    linkStyle default stroke:#385d8a,fill:none;
    linkStyle 3,5 stroke:#9bbb59,fill:none;
```

### VRE Modeler GUI


The VRE Modeler GUI must display information ranges from:
  - forms to instantiate a specific Facet according to the defined TemplateFacet;
  - list of instances to add to the scope, e.g.
    - EService;
    - RunningPlugin.
  - list of software to deploy e.g. 
    - DockerContainers;
    - OpenStack Images;
    - Plugins to deploy to instance(s) of a service. 
      For each software to deploy the service must find the action to invoke 
      (via ActionFacet) to allow to deploy such a software.

According to the user selection the service must be able to associate
the defined action (via ActionFacet) to invoke.

The Resource Manager APIs must return all the aggregated information to allow to design the GUI.


```json
{
    "type": "VirtualService",
    "id": "bd2a1f51-da7c-424c-8ce1-e510e8f928e9",
    "qualifier": null,
    "name": "Resource Catalogue",
    "description": "This functionality provides VRE members with a resource catalogue. All the Catalogue items are accompanied with rich descriptions capturing general attributes, e.g. title and creator(s); accessibility properties; technical properties, e.g. size and format; legal and ethical attributes, e.g. whether containing personal data; use and reuse policies, e.g. licences.",
    "optional": true,
    "version": "1.0.0",
    "group": "Data Discovery and Access",
    "mandatoryRelatedResources": [
        {
            "type": "ConfigurationTemplate",
            "id": "c006cc7e-9789-40a4-b676-a154fe0c63a1",
            "identificationType": "URI",
            "persistent": false,
            "value": "gcat-configuration",
            "newInstances": [
                {
                    "type": "TemplateFacet",
                    "name": "gcat-configuration",
                    "description": "Please create the configuration for gcat. Please note that ....",
                    "targetType": "SimpleFacet",
                    "properties": [
                        {
                            "type": "AttributeProperty",
                            "name": "socialPostEnabled",
                            "defaultValue": true,
                            "propertyType": "Boolean",
                            "readonly": false,
                            "notnull": false,
                            "mandatory": true
                        },
                        {
                            "type": "AttributeProperty",
                            "name": "portletURL",
                            "propertyType": "String",
                            "readonly": false,
                            "notnull": true,
                            "mandatory": true
                        },
                        {
                            "type": "AttributeProperty",
                            "name": "defaultOrganization",
                            "description": "The lower case versione of the VRE name should be the default Eg. for 'devVRE' we will have 'devvre'",
                            "propertyType": "String",
                            "readonly": false,
                            "notnull": true,
                            "mandatory": true
                        },
                        {
                            "type": "AttributeProperty",
                            "name": "publicPortletURL",
                            "propertyType": "String",
                            "readonly": false,
                            "notnull": true,
                            "mandatory": true
                        },
                        {
                            "type": "AttributeProperty",
                            "name": "notificationToUsersEnabled",
                            "propertyType": "Boolean",
                            "default": true,
                            "readonly": false,
                            "notnull": true,
                            "mandatory": true
                        },
                        {
                            "type": "AttributeProperty",
                            "name": "moderationEnabled",
                            "propertyType": "Boolean",
                            "default": false,
                            "readonly": false,
                            "notnull": true,
                            "mandatory": true
                        },
                        {
                            "type": "AttributeProperty",
                            "name": "supportedOrganizations",
                            "propertyType": "List<String>",
                            "readonly": false,
                            "notnull": true,
                            "mandatory": true
                        }
                    ]
                }
            ]
        }
    ],
    "derivatedRelatedResources": [
        {
            "name": "gCat service",
            "description": "The gCat service instances to use for the Resource Catalogue",
            "min": 1,
            "max": null,
            "instances": [
                {
                    "id": "",
                    "name": "gcat",
                    "group": "data-catalogue",
                    "version": "2.6.0",
                    "description": "This service allows any client to publish on the gCube Catalogue."
                }
            ]
        },
        {
            "name": "Ckan Instances",
            "description": "The Ckan instance to use for the Resource Catalogue",
            "min": 1,
            "max": 1,
            "instances": [
                {
                    "id": "8b664db9-dc32-4af9-9812-3ba794d2712d",
                    "name": "ckan"
                },
                {
                    "id": "2a360a1f-2889-40b5-af2b-129aa60c3ca3",
                    "name": "ckan"
                }
            ]
        }
    ]
}
```