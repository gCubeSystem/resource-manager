package org.gcube.resourcemanagement.context;

import java.util.UUID;

import org.gcube.common.security.secrets.Secret;
import org.gcube.informationsystem.contexts.reference.entities.Context;
import org.gcube.informationsystem.resourceregistry.api.exceptions.ResourceRegistryException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.contexts.ContextNotFoundException;
import org.gcube.resourcemanagement.utils.AuthorizationUtils;

import jakarta.ws.rs.InternalServerErrorException;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class RMTargetContext extends TargetContext {

    public RMTargetContext(){
        super();
    }

    public RMTargetContext(UUID uuid) throws ContextNotFoundException, ResourceRegistryException {
        super(uuid);
    }

    public RMTargetContext(String contextFullPath) throws ContextNotFoundException, ResourceRegistryException{
        super(contextFullPath);
    }

    public RMTargetContext(Context context) throws ResourceRegistryException {
        super(context);
    }

    @Override
    public Secret getSecret() {
        if(this.secret == null || this.secret.isExpired()){
            try {
                this.secret = AuthorizationUtils.getSecret(this.contextFullPath);
            } catch (Exception e) {
                throw new InternalServerErrorException("Unable to retrieve Application Token for context " + this.contextFullPath, e);
            }
        } 
        return this.secret;
    }

}
