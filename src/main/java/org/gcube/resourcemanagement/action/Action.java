package org.gcube.resourcemanagement.action;

/**
 * This interface is used to base class classes
 * implementing a specific action to be executed
 * when a resource having ActionFacet(s) is 
 * added/removed to/from a context.
 * @author Luca Frosini (ISTI - CNR)
 */
public interface Action {

}
