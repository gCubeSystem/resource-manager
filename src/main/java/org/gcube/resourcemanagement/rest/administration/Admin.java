package org.gcube.resourcemanagement.rest.administration;

import org.gcube.resourcemanagement.rest.BaseREST;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class Admin extends BaseREST {
	
	public static final String ADMIN_PATH = "admin";
}
