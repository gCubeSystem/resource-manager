package org.gcube.resourcemanagement.rest.administration;

import org.gcube.common.security.providers.SecretManagerProvider;
import org.gcube.resourcemanagement.annotation.PURGE;
import org.gcube.resourcemanagement.rest.BaseREST;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.webcohesion.enunciate.metadata.rs.ResourceGroup;
import com.webcohesion.enunciate.metadata.rs.ResourceLabel;
import com.webcohesion.enunciate.metadata.rs.ResponseCode;
import com.webcohesion.enunciate.metadata.rs.StatusCodes;
import com.webcohesion.enunciate.metadata.swagger.OperationId;

import jakarta.ws.rs.BadRequestException;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.DefaultValue;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.InternalServerErrorException;
import jakarta.ws.rs.PATCH;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.ResponseBuilder;
import jakarta.ws.rs.core.Response.Status;
import jakarta.xml.ws.WebServiceException;

/**
 * The Resource Manager configuration for the context of the request 
 * (i.e. the context where the token has been generated).
 * 
 * Only Managers are able to invoke non-safe methods.
 *  
 * @author Luca Frosini (ISTI - CNR)
 */
@Path(Admin.ADMIN_PATH + "/" + Configuration.CONFIGURATIONS_PATH)
@ResourceGroup("Administration APIs")
@ResourceLabel("Configuration APIs")
public class Configuration extends Admin {

	private static Logger logger = LoggerFactory.getLogger(Configuration.class);
	
	public static final String CONFIGURATIONS_PATH = "configurations";
	public static final String CURRENT_CONTEXT_PATH_PARAMETER = "CURRENT_CONTEXT";
	public static final String CONTEXT_FULLNAME_PARAMETER = "CONTEXT_FULLNAME_PARAMETER";
	
	protected String checkContext(String context) throws WebServiceException {
		if(context==null || context.compareTo("")==0) {
			throw new BadRequestException("Please provide a valid context as path parameter");
		}
		
		String c = SecretManagerProvider.get().getContext();
		if(context.compareTo(Configuration.CURRENT_CONTEXT_PATH_PARAMETER)==0) {
			return c; 
		}
		
		if(context.compareTo(c)!=0) {
			throw new BadRequestException("Context provided as path parameter (i.e. " + context + ") does not match with token request context (i.e. " + c + ")");
		}
		
		return c;
	}
	
	private String createOrUpdate() throws WebServiceException {
		return null;
	}
	
	/**
	 * This API allows to create the Resource Manager configuration for the 
	 * context of the request (i.e. the context where the token has been generated)
	 * using the json provided as request body.<br/>
	 */
	@POST
	@Consumes(BaseREST.APPLICATION_JSON_CHARSET_UTF_8)
	@Produces(BaseREST.APPLICATION_JSON_CHARSET_UTF_8)
	@StatusCodes ({
		@ResponseCode(code = 201, condition = "Resource Manager configuration successfully created."),
		@ResponseCode(code = 401, condition = "Only Manager can create the configuration."),
		@ResponseCode(code = 500, condition = "Error while persisting the configuration."),
	})
	public Response create(String json) throws WebServiceException {
		try {
			String ret = createOrUpdate();
			ResponseBuilder responseBuilder = Response.status(Status.CREATED);
			if(ret!=null) {
				responseBuilder.entity(ret).type(BaseREST.APPLICATION_JSON_CHARSET_UTF_8);
			}
			return responseBuilder.build();
		}catch (WebApplicationException e) {
			throw e;
		}catch (Exception e) {
			throw new InternalServerErrorException(e);
		}
	}
	
	/**
	 * This API allows to read the Resource Manager configuration for the 
	 * current context (i.e. the context where the token has been generated).<br/>
	 */
	@GET
	@Path("/{" + CONTEXT_FULLNAME_PARAMETER + "}")
	@Produces(BaseREST.APPLICATION_JSON_CHARSET_UTF_8)
	@StatusCodes ({
		@ResponseCode(code = 200, condition = "Resource Manager configuration successfully read."),
		@ResponseCode(code = 401, condition = "Only User with role Manager above can read the configuration."),
		@ResponseCode(code = 500, condition = "Error while reading the configuration."),
	})
	
	/**
	 * @param context
	 * @return
	 * @throws WebServiceException
	 */
	public Response read(@PathParam(CONTEXT_FULLNAME_PARAMETER) String context) throws WebServiceException {
		try {
			checkContext(context);
			return read();
		}catch (WebApplicationException e) {
			throw e;
		}catch (Exception e) {
			throw new InternalServerErrorException(e);
		}
	}
	
	public Response read() throws WebServiceException {
		try {
			String configuration = "{}";
			logger.debug("Configuration in context {} is {}", "", configuration);
			ResponseBuilder responseBuilder = Response.status(Status.OK);
			if(configuration!=null) {
				responseBuilder.entity(configuration).type(BaseREST.APPLICATION_JSON_CHARSET_UTF_8);
			}
			return responseBuilder.build();
		}catch (WebApplicationException e) {
			throw e;
		}catch (Exception e) {
			throw new InternalServerErrorException(e);
		}
	}
	
	/**
	 * This API allows to create/update the Resource Manager configuration for the 
	 * context of the request (i.e. the context where the token has been generated)
	 * using the json provided as request body.<br/>
	 */
	@PUT
	@Path("/{" + CONTEXT_FULLNAME_PARAMETER + "}")
	@Consumes(BaseREST.APPLICATION_JSON_CHARSET_UTF_8)
	@Produces(BaseREST.APPLICATION_JSON_CHARSET_UTF_8)
	@StatusCodes ({
		@ResponseCode(code = 200, condition = "Resource Manager configuration successfully created/updated."),
		@ResponseCode(code = 401, condition = "Only User with role Manager above can create/update the configuration."),
		@ResponseCode(code = 500, condition = "Error while creating/updating the configuration."),
	})
	@OperationId("Create or Update")
	public String createOrUpdate(@PathParam(CONTEXT_FULLNAME_PARAMETER) String context, String json) throws WebServiceException {
		try {
			return "{}";
		}catch (WebApplicationException e) {
			throw e;
		}catch (Exception e) {
			throw new InternalServerErrorException(e);
		}
	}
	
	public Response update(String json) throws WebServiceException {
		try {
			String configuration = "{}";
			logger.debug("Configuration in context {} has been updated to {}", "", configuration);
			ResponseBuilder responseBuilder = Response.status(Status.OK);
			if(configuration!=null) {
				responseBuilder.entity(configuration).type(BaseREST.APPLICATION_JSON_CHARSET_UTF_8);
			}
			return responseBuilder.build();
		}catch (WebApplicationException e) {
			throw e;
		}catch (Exception e) {
			throw new InternalServerErrorException(e);
		}
	}
	
	/**
	 * This API allows to patch the Resource Manager configuration for the 
	 * context of the request (i.e. the context where the token has been generated)
	 * using the json provided as request body.<br/>
	 */
	@PATCH
	@Path("/{" + CONTEXT_FULLNAME_PARAMETER + "}")
	@Consumes(BaseREST.APPLICATION_JSON_CHARSET_UTF_8)
	@Produces(BaseREST.APPLICATION_JSON_CHARSET_UTF_8)
	@StatusCodes ({
		@ResponseCode(code = 200, condition = "Resource Manager configuration successfully patched."),
		@ResponseCode(code = 401, condition = "Only User with role Manager above can patch the configuration."),
		@ResponseCode(code = 500, condition = "Error while patching the configuration."),
	})
	public Response patch(@PathParam(CONTEXT_FULLNAME_PARAMETER) String context, String json) throws WebServiceException {
		try {
			checkContext(context);
			return patch(json);
		}catch (WebApplicationException e) {
			throw e;
		}catch (Exception e) {
			throw new InternalServerErrorException(e);
		}
	}
	
	public Response patch(String json) throws WebServiceException {
		try {
			String ret = "{}";
			logger.debug("Configuration in context {} has been patched to {}", "", ret);
			ResponseBuilder responseBuilder = Response.status(Status.OK);
			if(ret!=null) {
				responseBuilder.entity(ret).type(BaseREST.APPLICATION_JSON_CHARSET_UTF_8);
			}
			return responseBuilder.build();
		}catch (WebApplicationException e) {
			throw e;
		}catch (Exception e) {
			throw new InternalServerErrorException(e);
		}
	}
	
	/**
	 * It removes from the cache the Resource Manager configuration for the
	 * context of the request (i.e. the context where the token has been generated).<br/>
	 * 
	 * This API forces the service to read again from the Information System (IS)
	 * the Resource Manager configuration for the context of the request.<br/>
	 * 
	 * If the user specifies the <code>purge</code> query parameter this API
	 * remove the configuration from the IS. Please note that this implies that 
	 * the Resource Manager is no more configured for the context of the request.
	 * 
	 * 
	 * @param context context must contains the context of the request 
	 * 	(i.e. the context where the token has been generated)
	 * 	or the placeholder <code>CURRENT_CONTEXT</code>.<br/>
	 * 	Please note that the context must be URL encoded, 
	 * 	e.g. /gcube/devsec/devVRE -> %2Fgcube%2Fdevsec%2FdevVRE
	 * @param purge indicates to the service to remove the configuration from the IS 
	 * @throws WebServiceException
	 * 
	 */
	@DELETE
	@Path("/{" + CONTEXT_FULLNAME_PARAMETER + "}")
//	@AuthorizationControl(allowedRoles={Role.CATALOGUE_MANAGER}, exception=NotAuthorizedException.class)
	@StatusCodes ({
		@ResponseCode(code = 200, condition = "Resource Manager configuration successfully deleted."),
		@ResponseCode(code = 401, condition = "Only User with role Manager above can delete the configuration."),
		@ResponseCode(code = 500, condition = "Error while deleting the configuration."),
	})
	public Response delete(@PathParam(CONTEXT_FULLNAME_PARAMETER) String context, 
			@QueryParam(BaseREST.PURGE_QUERY_PARAMETER) @DefaultValue("false") Boolean purge) throws WebServiceException {
		try {
			checkContext(context);
			if(purge) {
				return purge();
			}else {
				return delete();
			}
		}catch (WebApplicationException e) {
			throw e;
		}catch (Exception e) {
			throw new InternalServerErrorException(e);
		}
	}
	
	// Remove the Resource Manager configuration from cache and force reload
	public Response delete() throws WebServiceException {
		try {
			
			return Response.status(Status.NO_CONTENT).build();
		}catch (WebApplicationException e) {
			throw e;
		}catch (Exception e) {
			throw new InternalServerErrorException(e);
		}
	}
	
	/**
	 * It removes remove the Resource Manager configuration from the IS for the 
	 * context of the request (i.e. the context where the token has been generated).<br/>
	 * 
	 * Please note that this implies that 
	 * the resource manager is no more configured for the context of the request.
	 * 
	 * @param context context must contains the context of the request 
	 *  (i.e. the context where the token has been generated)
	 * 	or the placeholder <code>CURRENT_CONTEXT</code>.<br/>
	 * 	Please note that the context must be URL encoded, 
	 * 	e.g. /gcube/devsec/devVRE -> %2Fgcube%2Fdevsec%2FdevVRE
	 * @throws WebServiceException
	 * 
	 */
	@PURGE
	@Path("/{" + CONTEXT_FULLNAME_PARAMETER + "}")
//	@AuthorizationControl(allowedRoles={Role.CATALOGUE_MANAGER}, exception=NotAuthorizedException.class)
	@StatusCodes ({
		@ResponseCode(code = 200, condition = "Resource Manager configuration purged deleted."),
		@ResponseCode(code = 401, condition = "Only User with role Manager above can purge the configuration."),
		@ResponseCode(code = 500, condition = "Error while purging the configuration."),
	})
	public Response purge(@PathParam(CONTEXT_FULLNAME_PARAMETER) String context) throws WebServiceException {
		try {
			checkContext(context);
			return purge();
		}catch (WebApplicationException e) {
			throw e;
		}catch (Exception e) {
			throw new InternalServerErrorException(e);
		}
	}
	
	// Remove the configuration from cache and from IS
	public Response purge() throws WebServiceException {
		try {
			
			return Response.status(Status.NO_CONTENT).build();
		}catch (WebApplicationException e) {
			throw e;
		}catch (Exception e) {
			throw new InternalServerErrorException(e);
		}
	}
	
}
