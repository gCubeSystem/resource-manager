package org.gcube.resourcemanagement.rest;

import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@Provider
public class RMExceptionMapper implements ExceptionMapper<Exception> {
	
	@Override
	public Response toResponse(Exception exception) {
		
		Status status = Status.INTERNAL_SERVER_ERROR;
		String exceptionMessage = exception.getMessage();
		try {
			if(exception.getCause() != null) {
				exceptionMessage = exception.getCause().getMessage();
			}
		} catch(Exception e) {
			exceptionMessage = exception.getMessage();
		}
		MediaType mediaType = MediaType.TEXT_PLAIN_TYPE;
		
		if(WebApplicationException.class.isAssignableFrom(exception.getClass())) {
			Response gotResponse = ((WebApplicationException) exception).getResponse();
			status = Status.fromStatusCode(gotResponse.getStatusInfo().getStatusCode());
		}
		
		return Response.status(status).entity(exceptionMessage).type(mediaType).build();
	}
	
}
