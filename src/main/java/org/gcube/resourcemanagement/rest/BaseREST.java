package org.gcube.resourcemanagement.rest;

import org.gcube.smartgears.utils.InnerMethodName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.webcohesion.enunciate.metadata.rs.RequestHeader;
import com.webcohesion.enunciate.metadata.rs.RequestHeaders;

import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.HttpHeaders;
import jakarta.ws.rs.core.Response.ResponseBuilder;
import jakarta.ws.rs.core.UriInfo;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@RequestHeaders ({
	  @RequestHeader( name = "Authorization", description = "Bearer token, see <a href=\"https://dev.d4science.org/how-to-access-resources\">https://dev.d4science.org/how-to-access-resources</a>")
})
public class BaseREST {
	
	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	public static final String APPLICATION_JSON_CHARSET_UTF_8 = "application/json;charset=UTF-8";
	public static final String APPLICATION_JSON_API = "application/vnd.api+json";
	public static final String COUNT_KEY = "count";
	public static final String PURGE_QUERY_PARAMETER = "purge";
	
	@Context
	protected HttpHeaders httpHeaders;
	
	@Context
	protected UriInfo uriInfo;
	
	protected static final String LOCATION_HEADER = "Location";
	
	protected ResponseBuilder addLocation(ResponseBuilder responseBuilder, String id) {
		return responseBuilder.header(LOCATION_HEADER,
				String.format("%s/%s", uriInfo.getAbsolutePath().toString(), id));
	}
	
	protected String createCountJson(int count) {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("{\"");
		stringBuilder.append(COUNT_KEY);
		stringBuilder.append("\":");
		stringBuilder.append(count);
		stringBuilder.append("}");
		return stringBuilder.toString();
	}
	
	protected void setAccountingMethod(String method) {
		InnerMethodName.set(method);
		logger.info("{}", uriInfo.getAbsolutePath());
	}
	
//	protected void setAccountingMethod(Method method, String type) {
//		StringBuffer accountingMethod = new StringBuffer();
//		accountingMethod.append(method.getPrefix());
//		accountingMethod.append(type);
//		accountingMethod.append(method.getSuffix());
//		setAccountingMethod(accountingMethod.toString());
//	}
//	
//	private ServerRequestInfo initRequestInfo(ServerRequestInfo requestInfo) {
//		requestInfo.setUriInfo(uriInfo);
//		RequestUtility.getRequestInfo().set(requestInfo);
//		return requestInfo;
//	}
//	
//	protected ServerRequestInfo initRequestInfo(int offset, int limit) {
//		ServerRequestInfo requestInfo = new ServerRequestInfo(offset, limit);
//		return initRequestInfo(requestInfo);
//	}
//	
//	protected ServerRequestInfo initRequestInfo() {
//		ServerRequestInfo requestInfo = new ServerRequestInfo();
//		return initRequestInfo(requestInfo);
//	}
	
}
