package org.gcube.resourcemanagement.rest.resources;

import java.util.UUID;

import org.gcube.common.security.providers.SecretManagerProvider;
import org.gcube.common.security.secrets.Secret;
import org.gcube.informationsystem.contexts.reference.entities.Context;
import org.gcube.informationsystem.model.reference.entities.Resource;
import org.gcube.informationsystem.resourceregistry.api.exceptions.ResourceRegistryException;
import org.gcube.informationsystem.resourceregistry.api.rest.SharingPath;
import org.gcube.informationsystem.resourceregistry.api.rest.SharingPath.SharingOperation;
import org.gcube.resourcemanagement.analyser.InstanceAnalyser;
import org.gcube.resourcemanagement.analyser.InstanceAnalyserFactory;
import org.gcube.resourcemanagement.context.RMTargetContext;
import org.gcube.resourcemanagement.resource.Instance;
import org.gcube.resourcemanagement.rest.BaseREST;
import org.gcube.resourcemanagement.rest.vremodelling.ContextManager;

import jakarta.ws.rs.BadRequestException;
import jakarta.ws.rs.DefaultValue;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.WebApplicationException;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@Path(ResourceManager.RESOURCES_PATH_PART)
public class ResourceManager extends BaseREST {
	
	public static final String RESOURCES_PATH_PART = "resources";
	
	public static final String CURRENT_CONTEXT_PATH_PART = "CURRENT_CONTEXT";
	
	public static final String TYPE_PATH_PARAMETER = "TYPE_NAME";
	public static final String UUID_PATH_PARAMETER = "UUID";
	
	public ResourceManager() {
		super();
	}
	
	/*
	 * Allow to read the service definition 
	 * GET /services/{TYPE_NAME}/{UUID}
	 * e.g. GET /services/VirtualService/c0f314e7-2807-4241-a792-2a6c79ed4fd0
	 * 
	 */
	@GET
	@Path("{" + ResourceManager.TYPE_PATH_PARAMETER  + "}/{" + ResourceManager.UUID_PATH_PARAMETER + "}")
	@Produces(BaseREST.APPLICATION_JSON_CHARSET_UTF_8)
	public String get(@PathParam(ResourceManager.TYPE_PATH_PARAMETER) String type, 
						@PathParam(ResourceManager.UUID_PATH_PARAMETER) String uuid,
						@QueryParam(SharingPath.OPERATION_QUERY_PARAMETER) SharingOperation operation)
			throws WebApplicationException, ResourceRegistryException {
		logger.info("Requested to read {} with id {} ", type, uuid);
		
		setAccountingMethod("ReadResource");
		
		InstanceAnalyser<Resource, Instance> analyser = InstanceAnalyserFactory.getInstanceAnalyser(type, UUID.fromString(uuid));
		Instance instance = analyser.read(operation);
		return instance.toString();
	}
	
	/** 
	 * Add/Remove an instance from context. The operation can have a cascade effect due to propagation constraint.
	 * 
	 * Return the list of instances affected by an add/remove to/from context the target instance identified by UUID path parameter  
	 *  
	 * POST /sharing/contexts/{CONTEXT_UUID}/{TYPE_NAME}/{UUID}?operation=(ADD|REMOVE)&dryRun=true]
	 * 
	 * e.g 
	 * POST /resource-registry/sharing/contexts/67062c11-9c3a-4906-870d-7df6a43408b0/HostingNode/16032d09-3823-444e-a1ff-a67de4f350a8?operation=ADD&dryRun=true
	 * POST /resource-registry/sharing/contexts/67062c11-9c3a-4906-870d-7df6a43408b0/HostingNode/16032d09-3823-444e-a1ff-a67de4f350a8?operation=REMOVE&dryRun=true
	 * 
	 * where
	 * 67062c11-9c3a-4906-870d-7df6a43408b0 is the Context UUID and
	 * 16032d09-3823-444e-a1ff-a67de4f350a8 is the HostingNode UUID
	 * 
	 * The json contains the instantiation of templates
	 * 
	 */
	@POST
	@Path("/" + SharingPath.CONTEXTS_PATH_PART + "/{" + ContextManager.CONTEXT_UUID_PATH_PARAMETER + "}/"
		+ "{" + TYPE_PATH_PARAMETER + "}" + "/{" + UUID_PATH_PARAMETER + "}")
	@Produces(BaseREST.APPLICATION_JSON_CHARSET_UTF_8)
	public String addRemove(
			@PathParam(ContextManager.CONTEXT_UUID_PATH_PARAMETER) String contextId,
			@PathParam(TYPE_PATH_PARAMETER) String type,
			@PathParam(UUID_PATH_PARAMETER) String instanceId,
			@QueryParam(SharingPath.OPERATION_QUERY_PARAMETER) SharingOperation operation,
			@QueryParam(SharingPath.DRY_RUN_QUERY_QUERY_PARAMETER) @DefaultValue("false") Boolean dryRun,
			String json)
			throws WebApplicationException, ResourceRegistryException {
		
		StringBuffer calledMethod = new StringBuffer(); 
		if(dryRun==null) {
			dryRun = false;
		}
		
		if(dryRun) {
			calledMethod.append("dryRun");
		}
		
		if(operation == SharingOperation.ADD) {
			logger.info("Requested {} {} with UUID {} to {} with UUID {}", dryRun? "a dry run for adding": "to add", type, instanceId, Context.NAME, contextId);
			calledMethod.append("AddToContext");
		}else {
			logger.info("Requested {} {} with UUID {} from {} with UUID {}", dryRun? "a dry run for removing": "to remove", type, instanceId, Context.NAME, contextId);
			calledMethod.append("RemoveFromContext");
		}
		setAccountingMethod(calledMethod.toString());
		
		UUID instanceUUID = UUID.fromString(instanceId);
		InstanceAnalyser<Resource,Instance> analyser = InstanceAnalyserFactory.getInstanceAnalyser(type, instanceUUID);
		
		Secret secret = SecretManagerProvider.get();
		String currentContext = secret.getContext();

		UUID contextUUID = UUID.fromString(contextId);
		RMTargetContext targetContext = new RMTargetContext(contextUUID);
		String targetContextPath = targetContext.getContextFullPath();

		Instance instance = null;

		if(operation == SharingOperation.ADD) {
			analyser.setTargetContext(targetContext);
			instance = analyser.add();
			logger.trace("Instance added to context {} from context {}: {}", targetContextPath, currentContext, instance);
		}else if(operation == SharingOperation.REMOVE) {
			if(currentContext.compareTo(targetContextPath) != 0) {
				throw new BadRequestException("The request must be executed from the same context where the resource must be removed");
			}
			instance = analyser.remove();
			logger.trace("Instance removed from context {}: {}", currentContext, instance);	
		}

		return instance.toString();
	}
	
}
