package org.gcube.resourcemanagement.rest.vremodelling;

import java.io.IOException;
import org.gcube.informationsystem.contexts.reference.entities.Context;
import org.gcube.informationsystem.resourceregistry.api.exceptions.ResourceRegistryException;
import org.gcube.informationsystem.resourceregistry.api.rest.ContextPath;
import org.gcube.informationsystem.resourceregistry.contexts.ResourceRegistryContextClient;
import org.gcube.informationsystem.resourceregistry.contexts.ResourceRegistryContextClientFactory;
import org.gcube.informationsystem.serialization.ElementMapper;
import org.gcube.resourcemanagement.rest.BaseREST;

import jakarta.ws.rs.BadRequestException;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@Path(ContextManager.CONTEXTS_PATH_PART)
public class ContextManager extends BaseREST {
	
	public static final String CONTEXTS_PATH_PART = "contexts";

	public static final String CONTEXT_UUID_PATH_PARAMETER = "CONTEXT_UUID";

	public static final String CURRENT_CONTEXT_PATH_PART = "CURRENT_CONTEXT";
	
	public ContextManager() {
		super();
	}
	
	/*
	 * Create a new Context using the provided definition
	 * 
	 * POST /contexts
	 * 
	 * BODY: {...}
	 * 
	 */
	@POST
	@Path("{" + ContextManager.CONTEXT_UUID_PATH_PARAMETER + "}")
	@Consumes({MediaType.TEXT_PLAIN, BaseREST.APPLICATION_JSON_CHARSET_UTF_8})
	@Produces(BaseREST.APPLICATION_JSON_CHARSET_UTF_8)
	public String create(String json)
			throws ResourceRegistryException, WebApplicationException {
		logger.info("Requested to update/create {} with json {} ", Context.NAME, json);
		setAccountingMethod("Create Context");
		ResourceRegistryContextClient rrcc = ResourceRegistryContextClientFactory.create();
		return rrcc.create(json);
	}
	
	/*
	 * Allow to read the Context definition 
	 * GET /contexts/{UUID}
	 * e.g. GET /contexts/c0f314e7-2807-4241-a792-2a6c79ed4fd0
	 * 
	 */
	@GET
	@Path("{" + ContextManager.CONTEXT_UUID_PATH_PARAMETER + "}")
	@Produces(BaseREST.APPLICATION_JSON_CHARSET_UTF_8)
	public String read(@PathParam(ContextManager.CONTEXT_UUID_PATH_PARAMETER) String uuid)
			throws ResourceRegistryException, WebApplicationException {
		ResourceRegistryContextClient rrcc = ResourceRegistryContextClientFactory.create();
		if(uuid.compareTo(ContextPath.CURRENT_CONTEXT_PATH_PART)==0){
			logger.info("Requested to read current {}", Context.NAME);
			Context c = rrcc.readCurrentContext();
			try {
				return ElementMapper.marshal(c);
			}catch(Exception e){
				uuid = c.getID().toString();
			}
		}
		logger.info("Requested to read {} with id {} ", Context.NAME, uuid);
		setAccountingMethod("Read Context");
		return rrcc.read(uuid);
	}
	
	/*
	 * Update an existing Context definition
	 * 
	 * PUT /contexts/{UUID}
	 * e.g. PUT /contexts/c0f314e7-2807-4241-a792-2a6c79ed4fd0
	 * 
	 * BODY: {...}
	 * 
	 */
	@PUT
	@Path("{" + ContextManager.CONTEXT_UUID_PATH_PARAMETER + "}")
	@Consumes({MediaType.TEXT_PLAIN, BaseREST.APPLICATION_JSON_CHARSET_UTF_8})
	@Produces(BaseREST.APPLICATION_JSON_CHARSET_UTF_8)
	public String update(@PathParam(ContextManager.CONTEXT_UUID_PATH_PARAMETER) String uuid, String json)
			throws ResourceRegistryException, WebApplicationException {
		logger.info("Requested to update/create {} with UUID {} - Json {} ", Context.NAME, uuid, json);
		setAccountingMethod("Update Context");
		ResourceRegistryContextClient rrcc = ResourceRegistryContextClientFactory.create();
		try {
			Context c = ElementMapper.unmarshal(Context.class, json);
			if(c.getID().toString().compareTo(uuid)!=0){
				throw new BadRequestException("UUID provide as path parameter does not match with the ID of represented Context");
			}
		} catch (IOException e) {
			throw new BadRequestException("The Json representation of the context you provided is invalid");
		}
		return rrcc.update(json);
	}
	
	/*
	 * DELETE /contexts/{UUID}
	 * e.g. DELETE /contexts/c0f314e7-2807-4241-a792-2a6c79ed4fd0
	 */
	@DELETE
	@Consumes({MediaType.TEXT_PLAIN, BaseREST.APPLICATION_JSON_CHARSET_UTF_8})
	@Path("{" + ContextManager.CONTEXT_UUID_PATH_PARAMETER + "}")
	public Response delete(@PathParam(ContextManager.CONTEXT_UUID_PATH_PARAMETER) String uuid)
			throws ResourceRegistryException, WebApplicationException {
		logger.info("Requested to delete {} with id {} ", Context.NAME, uuid);
		setAccountingMethod("Delete Context");
		ResourceRegistryContextClient rrcc = ResourceRegistryContextClientFactory.create();
		rrcc.delete(uuid);
		return Response.status(Status.NO_CONTENT).build();
	}
	
}
