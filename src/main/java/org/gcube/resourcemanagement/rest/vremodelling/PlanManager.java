package org.gcube.resourcemanagement.rest.vremodelling;

import org.gcube.resourcemanagement.rest.BaseREST;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.WebApplicationException;

/**
 * This class is used to manage context plans.
 * When a new context is created a plan is provided to the Resource Manager.
 * The plan is a list of actions that the Resource Manager interprets to:
 * - add resources to context;
 * - create new instances (e.g. Virtual Services, IsRelatedTo relations);
 * - execute actions on resources (e.g. run the provisioning of a service).
 * @author Luca Frosini (ISTI - CNR)
 */
@Path(PlanManager.PLANS_PATH_PART)
public class PlanManager extends BaseREST {
	
	public static final String PLANS_PATH_PART = "plans";
	
	public static final String PLAN_UUID_PATH_PARAMETER = "PLAN_UUID";
	
	public PlanManager() {
		super();
	}
	
	/*
	 * Allow listing defined plans
	 * eventually with filters (pending, completed, failed)
	 * GET /plans
	 * e.g. GET /plans
	 * 
	 */
	@GET
	@Produces(BaseREST.APPLICATION_JSON_CHARSET_UTF_8)
	public String list() throws WebApplicationException {
		setAccountingMethod("List Plans");
		
		/*
		 * 
		 */
		
		return "[]";
	}
	
	/*
	 * Allow reading the plan definition 
	 * GET /plans/{UUID}
	 * e.g. GET /plans/c0f314e7-2807-4241-a792-2a6c79ed4fd0
	 * 
	 */
	@GET
	@Path("{" + PlanManager.PLAN_UUID_PATH_PARAMETER + "}")
	@Produces(BaseREST.APPLICATION_JSON_CHARSET_UTF_8)
	public String read(@PathParam(PlanManager.PLAN_UUID_PATH_PARAMETER) String uuid)
			throws WebApplicationException {
		logger.info("Requested to read Plan with id {} ", uuid);
		setAccountingMethod("Read Plan");
		
		/*
		 * Return a plan related to a Virtual Service
		 */
		
		return "{}";
	}
	
	
	/*
	 * Allow to read the plan definition 
	 * PUT /plans/{UUID}
	 * e.g. POST /plans/c0f314e7-2807-4241-a792-2a6c79ed4fd0
	 * Body
	 * 
	 * {}
	 * 
	 * It return the UUID of the plan
	 * 
	 */
	@PUT
	@Path("{" + PlanManager.PLAN_UUID_PATH_PARAMETER + "}")
	@Produces(BaseREST.APPLICATION_JSON_CHARSET_UTF_8)
	public String create(@PathParam(PlanManager.PLAN_UUID_PATH_PARAMETER) String uuid,
			String json) throws WebApplicationException {
		logger.info("Requested to create Plan with id {} ", uuid);
		setAccountingMethod("Create Plan");
		
		/*
		 * The json contains the definition of the service to add to a context
		 * (which already exist or has to be created)
		 * 
		 * According to the json information this service must be able to
		 * perform the associated actions.
		 * 
		 */
		
		
		
		// The UUID of the created plan
		return "";
	}
	
	/*
	 * Allow executing a plan
	 */
	@POST
	@Path("{" + PlanManager.PLAN_UUID_PATH_PARAMETER + "}")
	@Produces(BaseREST.APPLICATION_JSON_CHARSET_UTF_8)
	public String execute(@PathParam(PlanManager.PLAN_UUID_PATH_PARAMETER) String uuid,
			String json) throws WebApplicationException {
		
		/*
		 * Execute the plan and return the results
		 */
		
		return "";
	}
	
}
