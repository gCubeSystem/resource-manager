package org.gcube.resourcemanagement;

import org.gcube.common.security.providers.SecretManagerProvider;
import org.gcube.smartgears.ApplicationManager;
import org.gcube.smartgears.ContextProvider;
import org.gcube.smartgears.configuration.Mode;
import org.gcube.smartgears.context.application.ApplicationContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * This class is used to Manage the application initialization and shutdown per
 * context; The init and shutdown methods are called one per context in which
 * the app is running respectively at init and a shutdown time. It is connected
 * to the app declaring it via the @ManagedBy annotation (@see RMInitializer).
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
public class ResourceManager implements ApplicationManager {

	/**
	 * Logger
	 */
	private static Logger logger = LoggerFactory.getLogger(ResourceManager.class);
	
	public static boolean initialised;
	
	/** 
	 * {@inheritDoc}
	 */
	@Override
	public synchronized void onInit() {
		
		if (ContextProvider.get().container().configuration().mode() == Mode.offline) {
			logger.debug("Init called in offline mode");
			return;
		}
		
		String context = SecretManagerProvider.get().getContext();
		
		logger.trace(
				"\n-------------------------------------------------------\n"
				+ "Resource Manager is Starting on context {}\n"
				+ "-------------------------------------------------------",
				context);
		
		ApplicationContext applicationContext = ContextProvider.get();
		String rmEServiceID  = applicationContext.id();
		logger.info("Resource Manager has the following ID {}", rmEServiceID);
		
		
		logger.trace(
				"\n-------------------------------------------------------\n"
				+ "Resource Manager Started Successfully on context {}\n"
				+ "-------------------------------------------------------",
				context);
		
	}
	
	/** 
	 * {@inheritDoc} 
	 */
	@Override
	public synchronized void onShutdown(){
		if (ContextProvider.get().container().configuration().mode() == Mode.offline) {
			logger.debug("Init called in offline mode");
			return;
		}
		
		String context = SecretManagerProvider.get().getContext();
		
		logger.trace(
				"\n-------------------------------------------------------\n"
				+ "Resource Manager is Stopping on context {}\n"
				+ "-------------------------------------------------------", 
				context);
		

		
		logger.trace(
				"\n-------------------------------------------------------\n"
				+ "Resource Manager Stopped Successfully on context {}\n"
				+ "-------------------------------------------------------", 
				context);
	}
}
