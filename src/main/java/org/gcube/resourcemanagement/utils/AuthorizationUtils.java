package org.gcube.resourcemanagement.utils;

import java.io.InputStream;
import java.net.URL;
import java.util.AbstractMap.SimpleEntry;
import java.util.Map.Entry;
import java.util.Properties;

import org.gcube.common.iam.D4ScienceIAMClient;
import org.gcube.common.iam.D4ScienceIAMClientAuthn;
import org.gcube.common.security.secrets.AccessTokenSecret;
import org.gcube.common.security.secrets.Secret;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.ws.rs.InternalServerErrorException;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class AuthorizationUtils {
	
	private static final Logger logger = LoggerFactory.getLogger(AuthorizationUtils.class);
	
	protected static final String CLIENT_ID_SECRET_FILENAME = "config.properties";
	protected static final String CLIENT_ID_PROPERTY_NAME = "clientId";
	
	private static Entry<String, String> getClientIdAndClientSecret(String context) {
		try {
			Properties properties = new Properties();
			ClassLoader classLoader = AuthorizationUtils.class.getClassLoader();
			URL url = classLoader.getResource(CLIENT_ID_SECRET_FILENAME);
			logger.trace("Going to read {} at {}", CLIENT_ID_SECRET_FILENAME, url.toString());
			InputStream input = classLoader.getResourceAsStream(CLIENT_ID_SECRET_FILENAME);
			properties.load(input);
			
			String clientId = null;
			if(properties.containsKey(CLIENT_ID_PROPERTY_NAME)) {
				clientId = properties.getProperty(CLIENT_ID_PROPERTY_NAME);
			}else{
				throw new InternalServerErrorException("Unable to retrieve " + CLIENT_ID_PROPERTY_NAME);
			}
			
			int index = context.indexOf('/', 1);
			String root = context.substring(0, index == -1 ? context.length() : index);
			String clientSecret = properties.getProperty(root);
			
			SimpleEntry<String, String> entry = new SimpleEntry<String, String>(clientId, clientSecret);
			return entry;
		} catch(Exception e) {
			throw new InternalServerErrorException("Unable to retrieve Application Token for context " + context, e);
		}
	}
	
	private static String getAccessTokenString(String clientId, String clientSecret, String context) throws Exception {
		int index = context.indexOf('/', 1);
		String root = context.substring(0, index == -1 ? context.length() : index);
		D4ScienceIAMClient iamClient = D4ScienceIAMClient.newInstance(root);
		D4ScienceIAMClientAuthn d4ScienceIAMClientAuthn = iamClient.authenticate(clientId, clientSecret, context);
		return d4ScienceIAMClientAuthn.getAccessTokenString();
	}
	
	public static Secret getSecret(String context) throws Exception {
		Entry<String,String> entry = getClientIdAndClientSecret(context);
		String accessToken = getAccessTokenString(entry.getKey(), entry.getValue(), context);
		Secret secret = new AccessTokenSecret(accessToken, context);
		return secret;
	}
	
}
