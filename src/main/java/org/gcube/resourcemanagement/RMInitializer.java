package org.gcube.resourcemanagement;

import org.gcube.resourcemanagement.rest.BaseREST;
import org.gcube.resourcemanagement.rest.administration.Admin;
import org.gcube.resourcemanagement.rest.vremodelling.ContextManager;
import org.gcube.smartgears.annotations.ManagedBy;
import org.glassfish.jersey.server.ResourceConfig;

import jakarta.ws.rs.ApplicationPath;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@ApplicationPath("/")
@ManagedBy(ResourceManager.class)
public class RMInitializer extends ResourceConfig {
	
	public RMInitializer() {
		packages(BaseREST.class.getPackage().toString());
		packages(Admin.class.getPackage().toString());
		packages(ContextManager.class.getPackage().toString());
	}
	
}
